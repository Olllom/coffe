#!/usr/bin/env bash
#SBATCH --nodes=1 --exclusive
#SBATCH --time 12:00:00
#SBATCH --mem 185G
#SBATCH --partition hpc3
#SBATCH --output slurm.%N.%j.out # filename for STDOUT (%N: nodename, %j: job-ID)
#SBATCH --error slurm.%N.%j.err  # filename for STDERR
module purge
module load gromacs/2018.2
module load gcc/7.3.0
module load cuda/9.2

export GMXLIB=/usr/local/hpc1/software_molmod/gromacs/forcefields
