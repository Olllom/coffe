#!/bin/bash

#SBATCH --partition=hpc3         # partition (queue)
#SBATCH --nodes=1 --exclusive	 # number of nodes
#SBATCH --mem=180G               # memory per node in MB (different units with suffix K|M|G|T)
#SBATCH --time=59:00             # total runtime of job allocation ((format D-HH:MM:SS; first parts optional)

#auskommentiert_test
#SBATCH --output=slurm.%j.out    # STDOUT
#SBATCH --error=slurm.%j.err     # STDERR
