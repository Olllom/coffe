# -*- coding: utf-8 -*-

""" This module calculates the radius of gyration of an entity.
    Requires pytraj (python) from AMBER MD software
"""

from __future__ import absolute_import, division, print_function

import os
import pandas
import pytraj

from coffe.core import coffedir


def gyration_radius_csv(inputfiles=None, outputfile=None, work_dir=None):
    """
    Compute the radius of gyration for individual structures (i.e. not from an
        MD trajectory)
    Returns: a csv file containing structure source and resulting data values
        rounded to 3rd decimal

    Requires a good structure files (e.g. a PDB or MOL2 (e.g. made using
        openbabel) -- please see
        http://amber-md.github.io/pytraj/latest/read_and_write.html

    Args:
        inputfiles (list): A list of [pdb|mol2] structures
        outputfile (str): Output csv file name (default: gyrad.csv)
        work_dir (str): working directory (default: current working directory)

    Returns:
        1. A csv file.

    Examples: gyration_radius.gyration_radius_csv(inputfiles, outputfile)
              gyration_radius.gyration_radius_csv(inputfiles, outputfile, work_dir)

    Raises:
        TypeError: inputfiles arg is not a list.
        TypeError: outputfile arg is not a string.
        TypeError: work_dir arg is not a string.
        AssertionError: if one of the input molecules does not exist.
    """
    if work_dir is None:
        work_dir = os.getcwd()

    with coffedir.CoffeWorkDir(work_dir,
                               gyration_radius_csv.__name__
                               + ": Computing radius of gyration.",
                               locals()) as cwd:

        if inputfiles is None:
            inputfiles = []
        if outputfile is None:
            outputfile = 'gyrad.csv'

        if not isinstance(inputfiles, list):
            raise TypeError('The provided structure argument is not a list.')
        for file in inputfiles:
            assert os.path.isfile(
                file), "Input file ({0}) file doesn't exist.".format(str(file))
            if not file.lower().endswith(('.pdb', '.mol2')):
                raise TypeError(
                    'The provided file type ({0}) is not allowed.'.format(
                        str(file)))
        if not isinstance(outputfile, str):
            raise TypeError('The output file name is not a string.')
        if not isinstance(work_dir, str):
            raise TypeError(
                'The working directory ({0}) is not a string.'.format(work_dir))

        outputfile = os.path.join(work_dir, outputfile)

        top = pytraj.load_topology(inputfiles[0])
        molecules_pytraj = pytraj.iterload(inputfiles, inputfiles[0])
        gyrad = pytraj.radgyr(molecules_pytraj, top=top)
        gyrad_rounded = [round(elem, 3) for elem in gyrad]

        for index, value in enumerate(inputfiles):
            inputfiles[index] = os.path.basename(value)

        precision = 3
        gyrad_file = pandas.DataFrame(
            {"gyration radius": gyrad.round(decimals=precision)})
        gyrad_file.insert(loc=0, column='entity', value=inputfiles)
        gyrad_file.to_csv(outputfile, header=True, index=False,
                          index_label=inputfiles, float_format='%.3f')

        return gyrad_rounded
