# -*- coding: utf-8 -*-

"""Extract raw final energies and save them to files."""

from __future__ import absolute_import, division, print_function

import collections
import csv
import glob
import os

from coffe.core import coffedir
from coffe.misc import get_program_name
import coffe.misc.util as miscutil


def grab_last_item_in_line(inputline=None, work_dir=os.getcwd()):
    """
    Take a line (e.g. a sentence), convert to a list, and returns the last item (i.e. the last word).

    Args:
        inputline (str): An input line
        work_dir (str): current working directory
    Returns:
        lastitem (str): The last item in the input line
    Raises:
        TypeError: if inputline is not a string
    """
    with coffedir.CoffeWorkDir(work_dir, grab_last_item_in_line.__name__ + ": Grabbing lines.",
                               locals()) as cwd:
        if not isinstance(inputline, str):
            raise TypeError('The value for input line is not a string.')

        lineaslist = inputline.split()
        if "[Eh]" in lineaslist:
            lineaslist.remove("[Eh]")
        lastitem = lineaslist[-1]
        return lastitem


# def extract_amber_energies(amberlog):
#     """Extract raw final energies from AMBER constraint minimizations"""
#
#     start = 'FINAL RESULTS'
#     end = 'BOND'
#     lines = []
#     amber_energies = []
#     with open(amberlog) as input_data:
#         for line in input_data:
#             if line.strip() == start:
#                 break
#         # Reads text until the end of the block:
#         for line in input_data:
#             if line.strip() == end:
#                 break
#             lines.append(line.split())
#     lines = [x for x in lines if x]  # remove empty lists
#     amber_energies.append(lines[1][1])
#
#     return amber_energies
#
#
# def extract_gromacs_energies(gromacslog):
#     """Extract raw final energies from GROMACS constraint minimizations"""
#
#     start = 'Potential Energy  ='
#     end = 'Maximum force'
#     lines = []
#     gromacs_energies = []
#     with open(gromacslog) as input_data:
#         for line in input_data:
#             if line.strip() == start:
#                 break
#         # Reads text until the end of the block:
#         for line in input_data:
#             if line.strip() == end:
#                 break
#             lines.append(line.split('='))
#     lines = [x for x in lines if x]
#     gromacs_energies.append(lines)
#
#     return gromacs_energies


def extract_gamess_energies(inputfile=None, longpath=None, work_dir=os.getcwd()):
    """
    Extract energies from a GAMESS log file. Should captures lines for:
    HF and MP2.

    Args:
        inputfile (str): A GAMESS log file
        work_dir (str): current working directory
    Returns:
        return_energies (ord. dict): An ordered dictionary containing source filename, theory (e.g. HF, MP2) and raw
        energy --> e.g. [('File', 'molecule-2-psi.log'), ('HF', '-157.2968813194237043')]
    Raises:
        TypeError: if gamesslog is not a string
    """
    if not isinstance(inputfile, str):
        raise TypeError('The value for input GAMESS log file is not a string.')

    with coffedir.CoffeWorkDir(work_dir, extract_gamess_energies.__name__ + ": Extracting GAMESS energies.",
                               locals()) as cwd:

        with open(inputfile, "r") as logdata:
            loglines = logdata.readlines()

        if any("MPLEVL=2" in x for x in loglines):
            lookupline = ['E(SCF)=', 'E(MP2)=']
        else:
            lookupline = ['TOTAL ENERGY =']

        hf_collected_energies = collections.OrderedDict()
        mp2_1_collected_energies = collections.OrderedDict()
        file = collections.OrderedDict()

        for lookup in lookupline:
            start = [i for i, s in enumerate(loglines) if lookup in s]

            if len(start) > 0:
                extractline = loglines[start[-1]]
                energy = grab_last_item_in_line(extractline)
                if lookup == 'E(SCF)=':
                    hf_collected_energies['HF'] = energy
                elif lookup == 'TOTAL ENERGY =':
                    hf_collected_energies['HF'] = energy
                if lookup == 'E(MP2)=':
                    mp2_1_collected_energies['MP2'] = energy

        all_energies = miscutil.merge_two_ordered_dictionaries(hf_collected_energies, mp2_1_collected_energies)

        if longpath is False:
            file['File'] = os.path.basename(inputfile)
        else:
            file['File'] = inputfile

        energies = miscutil.merge_two_ordered_dictionaries(file, all_energies)

        return energies


def extract_psi4_energies(inputfile=None, longpath=None, work_dir=os.getcwd()):
    """ Extract energies from Psi4 log file. It should work for:
    HF, DF-CCSD(T), DF-MP2/CBS, DF-MP2.5, or DF-MP2.

    Args:
        inputfile (str): A Psi4 log file
        longpath (boolian): Return full path of files
        work_dir (str): current working directory
    Returns:
        return_energies (ord. dict): An ordered dictionary containing source filename, theory (e.g. HF, MP2) and raw
        energy  --> e.g. [('File', 'molecule-2-psi.log'), ('HF', '-157.2968813194237043')]
    Raises:
        TypeError: if psi4log is not a string
    """

    # TODO: full energy list can be seen http://www.psicode.org/psi4manual/master/_modules/psi4/driver/driver_cbs.html

    with coffedir.CoffeWorkDir(work_dir, extract_psi4_energies.__name__ + ": Extracting Psi4 energies.",
                               locals()) as cwd:

        if not isinstance(inputfile, str):
            raise TypeError('The value for input Psi4 log file is not a string.')

        with open(inputfile, "r") as logdata:
            loglines = logdata.readlines()

        # Check to see what type of calculation
        if any("CBS Results: custom function" in x for x in loglines):
            lookupline = ['total                  CBS']
        elif any("CCSD(T) total energy" in x for x in loglines):
            lookupline = ['@DF-RHF Final Energy', 'MP2 total energy', 'CCSD total energy', 'CCSD(T) total energy']
        elif any("DF-MP2.5 Total Energy" in x for x in loglines):
            lookupline = ['DF-HF Energy', 'DF-MP2 Total Energy', 'DF-MP3 Total Energy', 'DF-MP2.5 Total Energy']
        elif any("DF-MP2" in x for x in loglines):
            lookupline = ['Reference Energy', 'Total Energy              =']
        elif any("DFT Potential" in x for x in loglines):
            lookupline = ['Final energy is']
        else:
            lookupline = ['@DF-RHF Final Energy']

        hf_collected_energies = collections.OrderedDict()
        mp2_collected_energies = collections.OrderedDict()
        mp25_collected_energies = collections.OrderedDict()
        mp3_collected_energies = collections.OrderedDict()
        dft_collected_energies = collections.OrderedDict()
        cssd_collected_energies = collections.OrderedDict()
        cssdt_collected_energies = collections.OrderedDict()
        cbs_collected_energies = collections.OrderedDict()

        file = collections.OrderedDict()
        all_energies = collections.OrderedDict()

        for lookup in lookupline:
            start = [i for i, s in enumerate(loglines) if lookup in s]

            if len(start) > 0:
                extractline = loglines[start[-1]]
                energy = grab_last_item_in_line(extractline)
                if lookup == '@DF-RHF Final Energy':
                    hf_collected_energies['HF'] = energy
                elif lookup == 'DF-HF Energy':
                    hf_collected_energies['HF'] = energy
                elif lookup == 'Reference Energy':
                    hf_collected_energies['HF'] = energy
                elif lookup == '@DF-RHF Final Energy':
                    hf_collected_energies['HF'] = energy

                if lookup == 'Final energy is':
                    mp2_collected_energies['DFT'] = energy

                if lookup == 'MP2 total energy':
                    mp2_collected_energies['MP2'] = energy
                elif lookup == 'DF-MP2 Total Energy':
                    mp2_collected_energies['MP2'] = energy
                elif lookup == 'Total Energy              =':
                    mp2_collected_energies['MP2'] = energy

                if lookup == 'CCSD(T) total energy':
                    cssdt_collected_energies['CCSDT'] = energy
                elif lookup == 'CCSD total energy':
                    cssd_collected_energies['CCSD'] = energy

                if lookup == 'DF-MP3 Total Energy':
                    mp3_collected_energies['MP3'] = energy
                if lookup == 'DF-MP2.5 Total Energy':
                    mp25_collected_energies['MP25'] = energy
                if lookup == 'total                  CBS':
                    cbs_collected_energies['CBS'] = energy

        all_energies = miscutil.merge_two_ordered_dictionaries(hf_collected_energies, mp2_collected_energies)
        all_energies = miscutil.merge_two_ordered_dictionaries(all_energies, mp3_collected_energies)
        all_energies = miscutil.merge_two_ordered_dictionaries(all_energies, mp25_collected_energies)
        all_energies = miscutil.merge_two_ordered_dictionaries(all_energies, dft_collected_energies)
        all_energies = miscutil.merge_two_ordered_dictionaries(all_energies, cssd_collected_energies)
        all_energies = miscutil.merge_two_ordered_dictionaries(all_energies, cssdt_collected_energies)
        all_energies = miscutil.merge_two_ordered_dictionaries(all_energies, cbs_collected_energies)

        if longpath is False:
            file['File'] = os.path.basename(inputfile)
        else:
            file['File'] = inputfile
        energies = miscutil.merge_two_ordered_dictionaries(file, all_energies)
        return energies


def extract_energies_and_write(inputfiles=None, outputfile=None, program=None, longpath=None, work_dir=os.getcwd()):
    """This is the main program for extracting the final energy of QM optimization runs or single point calculations.
    Note that this uses "Human Sorting" of the log files, via the natsort library.

    Args:
        inputfiles (list): A list of QM (eg. GAMESS, Psi4) log file
        outputfile (str): The name of the generated output csv file (default = energies_raw.csv)
        program (str): program that generated the QM logfiles (default = guess from log file content)
        longpath (boolian): return full path of files (default = False)
        work_dir (str): current working directory
    Returns:
        return_energies (ord. dict): a csv formatted output_file that contains the file names and raw energies
    Raises:
        Exceptions: if output csv already exists
        Exception: if no QM log file is present in working directory
        TypeError: if the value of the inputfiles is not a list
        TypeError: if outputfilename is not a string
        TypeError: if program is not a string
    """
    with coffedir.CoffeWorkDir(work_dir, extract_energies_and_write.__name__ + ": Extracting QM energies and writing"
                                                                               "a csv file.", locals()) as cwd:
        if inputfiles is None or inputfiles[0] == 'all' or inputfiles[0] == 'All':
            inputfiles = []
            source = os.path.join(work_dir, "*.log")
            inputfiles = glob.glob(source)
        if outputfile is None:
            outputfile = 'energies_raw.csv'

        if longpath is None:
            longpath = False

        if os.path.isfile(str(outputfile)):
            raise Exception("{} is already present. Exiting to avoid overwriting.".format(outputfile))

        assert len(inputfiles) > 0, 'There are no inputfiles - {}.'.format(extract_energies_and_write.__name__)

        if program is None:
            program, progabbrev = get_program_name.set_program_abbrev_guess(inputfiles[0])
        else:
            program, progabbrev = get_program_name.set_program_abbrev_user(program)

        if not isinstance(inputfiles, list):
            raise TypeError('The value for inputfiles is not a list.')
        if not isinstance(outputfile, str):
            raise TypeError('The value for output filename is not a string.')
        if not isinstance(program, str):
            raise TypeError('The value for the source program is not a string.')
        if not isinstance(work_dir, str):
            raise TypeError('The value for work_dir is not a string.')

        for log in inputfiles:
            assert os.path.isfile(log), "Input file ({0}) doesn't exist.".format(str(log))
            # optimized = check_qm_log.check_qm_optimization(log, program) ## doesnt work for SPC calculations
            # if optimized == 'True':
            #    inputfiles.remove(log)

        outputfile = os.path.join(work_dir, outputfile)

        qm_logs_natural_sorted = miscutil.natural_sort(inputfiles)
        qm_logs = qm_logs_natural_sorted

        keynames = []
        energies_dict = dict()

        for log in qm_logs:
            if program == 'psi4':
                energies_dict = extract_psi4_energies(log, longpath)
            elif program == 'gamess':
                energies_dict = extract_gamess_energies(log, longpath)
            energies = [energies_dict]

            with open(outputfile, 'a') as f:
                writer = csv.DictWriter(f, energies[0].keys())
                for e in energies:
                    writer.writerow(e)

        for key, value in energies_dict.items():
            keynames.append(key)

        # Reverse ordering in order to properly add column headers
        with open(str(outputfile)) as fi, open('{0}/energies_raw.temp'.format(work_dir), 'w') as fo:
            fo.writelines(reversed(fi.readlines()))

        # Add column headers (i.e. keys)
        with open('{0}/energies_raw.temp'.format(work_dir), 'a') as f:
            writer = csv.writer(f)
            writer.writerow(keynames)

        # Undo the above reverse to re-obtain proper sequence.
        with open('{0}/energies_raw.temp'.format(work_dir)) as fi, open(str(outputfile), 'w') as fo:
            fo.writelines(reversed(fi.readlines()))

        os.remove('{0}/energies_raw.temp'.format(work_dir))
