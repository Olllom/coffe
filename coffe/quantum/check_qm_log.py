# -*- coding: utf-8 -*-

"""Check if the QM log results in an optimization or not."""

from __future__ import absolute_import, division, print_function

import os
from coffe.core import coffedir
from coffe.misc import get_program_name


def define_qm_error(inputfile=None, program=None, work_dir=os.getcwd()):
    """Search QM log files for what error might have occurred."""

    with coffedir.CoffeWorkDir(work_dir, "Trying to define what the error might be.", locals()) as cwd:
        if inputfile is None:
            raise IOError("The inputfile was not specified.")
        if program is None:
            program, progabbrev = get_program_name.set_program_abbrev_guess(inputfile)
        else:
            program, progabbrev = get_program_name.set_program_abbrev_user(program)

        if not isinstance(inputfile, str):
            raise TypeError('The output file name is not a string.')
        if not isinstance(program, str):
            raise TypeError('The working directory is not a string.')
        if not isinstance(work_dir, str):
            raise TypeError('The value for work_dir is not a string.')
        assert os.path.isfile(inputfile), "Input file ({0}) doesn't exist.".format(str(inputfile))

        error = 'Unknown Error'

        if progabbrev == 'psi':
            lookupline = ['OptimizationConvergenceError: Could not converge geometry optimization',
                          'OptimizationConvergenceError: Could not converge geometry optimization in 0 iterations.']

        with open(inputfile) as input_data:
            for line in input_data:
                for lookup in lookupline:
                    if lookup not in line.strip():
                        continue
                    else:
                        error = lookup

        return error


def check_qm_optimization(inputfile=None, program=None, work_dir=os.getcwd()):
    """Function that checks if QM optimization completed or not.

    Looks for one of the following phrases:
    Gamess -- "EQUILIBRIUM GEOMETRY LOCATED"
    Psi4 -- "Optimization is complete!"

    Args:
        inputfile: input QM log file
        program: Quantum program that created logfile, which can be detected
                    automatically

    Returns:
        optimized: "True" or "False"

    Raises:
        TypeError: if inputfile is not a string
        TypeError: if program is not a string
        TypeError: if work_dir is not a string
        IOError: If the inputfile is not provided.
        IOError: If the inputfile is not found.
    """

    with coffedir.CoffeWorkDir(work_dir, "Checking if QM log files has an optimized structure.", locals()) as cwd:

        if inputfile is None:
            raise IOError("The inputfile was not specified.")
        if program is None:
            program, progabbrev = get_program_name.set_program_abbrev_guess(inputfile)
        else:
            program, progabbrev = get_program_name.set_program_abbrev_user(program)

        if not isinstance(inputfile, str):
            raise TypeError('The output file name is not a string.')
        if not isinstance(program, str):
            raise TypeError('The working directory is not a string.')
        if not isinstance(work_dir, str):
            raise TypeError('The value for work_dir is not a string.')
        assert os.path.isfile(inputfile), "Input file ({0}) doesn't exist.".format(str(inputfile))

        success = "False"

        if progabbrev == 'gam':
            lookupline = ['EQUILIBRIUM GEOMETRY LOCATED', 'SADDLE POINT LOCATED']
        elif progabbrev == 'psi':
            lookupline = ['Optimization is complete!']

        with open(inputfile) as input_data:
            for line in input_data:
                for lookup in lookupline:
                    if lookup not in line.strip():
                        continue
                    else:
                        success = 'True'

        return success

def check_qm_spc(inputfile=None, program=None, work_dir=os.getcwd()):
    """Function that checks if QM optimization completed or not.

    Looks for one of the following phrases:
    Gamess -- "EQUILIBRIUM GEOMETRY LOCATED"
    Psi4 -- "Optimization is complete!"

    Args:
        inputfile: input QM log file
        program: Quantum program that created logfile, which can be detected
                    automatically

    Returns:
        optimized: "True" or "False"

    Raises:
        TypeError: if inputfile is not a string
        TypeError: if program is not a string
        TypeError: if work_dir is not a string
        IOError: If the inputfile is not provided.
        IOError: If the inputfile is not found.
    """

    with coffedir.CoffeWorkDir(work_dir, "Checking if QM log files has an optimized structure.", locals()) as cwd:

        if inputfile is None:
            raise IOError("The inputfile was not specified.")
        if program is None:
            program, progabbrev = get_program_name.set_program_abbrev_guess(inputfile)
        else:
            program, progabbrev = get_program_name.set_program_abbrev_user(program)

        if not isinstance(inputfile, str):
            raise TypeError('The output file name is not a string.')
        if not isinstance(program, str):
            raise TypeError('The working directory is not a string.')
        if not isinstance(work_dir, str):
            raise TypeError('The value for work_dir is not a string.')
        assert os.path.isfile(inputfile), "Input file ({0}) doesn't exist.".format(str(inputfile))

        success = "False"

        if progabbrev == 'gam':
            lookupline = ['DONE WITH MP2 ENERGY']
        elif progabbrev == 'psi':
            lookupline = ['CBS Results: custom function',
                          '==================> DF-MP2 Energies <====================',
                          'MP2.5 FINAL RESULTS',
                          'CCSD(T) total energy']

        with open(inputfile) as input_data:
            for line in input_data:
                for lookup in lookupline:
                    if lookup not in line.strip():
                        continue
                    else:
                        success = "True"

        return success
