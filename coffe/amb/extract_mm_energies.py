# -*- coding: utf-8 -*-

"""Extract raw final energies and save them to files."""

from __future__ import absolute_import, division, print_function

import collections
import csv
import glob
import os

from coffe.misc import get_program_name
import coffe.misc.util as miscutil


def extract_amber_energies(amberout=None, forcefield=None):
    """Extract raw final energies from AMBER constraint minimizations."""
    if not isinstance(amberout, str):
        raise TypeError('The value for amberout ({0}) is not a string.'.format(amberout))
    if not isinstance(forcefield, str):
        raise TypeError('The value for forcefield ({0}) is not a string.'.format(forcefield))

    start = 'FINAL RESULTS'
    end = 'BOND'
    lines = []

    amber_energy = collections.OrderedDict()
    dictfile = collections.OrderedDict()

    with open(amberout) as input_data:
        for line in input_data:
            if line.strip() == start:
                break
        for line in input_data:
            if line.strip() == end:
                break
            lines.append(line.split())
    lines = [x for x in lines if x]  # remove empty lists

    key = forcefield
    amber_energy[key] = lines[1][1]
    dictfile['File'] = os.path.basename(amberout)
    final_dictionary = miscutil.merge_two_ordered_dictionaries(dictfile, amber_energy)
    return final_dictionary


# def extract_gromacs_energies(gromacslog):
#     """Extract raw final energies from GROMACS constraint minimizations"""
#
#     start = 'Potential Energy  ='
#     end = 'Maximum force'
#     lines = []
#     gromacs_energies = []
#     with open(gromacslog) as input_data:
#         for line in input_data:
#             if line.strip() == start:
#                 break
#         # Reads text until the end of the block:
#         for line in input_data:
#             if line.strip() == end:
#                 break
#             lines.append(line.split('='))
#     lines = [x for x in lines if x]
#     gromacs_energies.append(lines)
#
#     return gromacs_energies


def extract_energies_and_write(source_dir=None, write_dir=None, filename=None, forcefield='MM', program=None):
    """This is the main program for extracting the final energy of
    optimization runs. Note that this uses "Human Sorting" of the
    log files, via the natsort library.

    Currently, this can call the following function:
    1) extract_amber_energies

    Requires:
    1) a directory_target for where the log files exist (default = cwd)
    2) a write_directory for where the output csv file should be written (default = cwd)
    3) an output_file for the name of the output csv file (default = guessing from log file)
    4) the program name that created the energy (default = Energies_raw.csv)

    Returns: csv formatted output_file that contains the file names and raw energies
    """
    if source_dir is None:
        source_dir = os.getcwd()
    if write_dir is None:
        write_dir = os.getcwd()
    if filename is None:
        filename = str(write_dir) + '/Energies_raw.csv'

    mm_outs = glob.glob(source_dir + "/*.min.out")

    try:
        mm_outs[0]
    except IndexError:
        raise Exception("There are no out files present here. Exiting")

    if program is None:
        program, progabbrev = get_program_name.set_program_abbrev_guess(mm_outs[0])
    else:
        program, progabbrev = get_program_name.set_program_abbrev_user(program)

    mm_logs_natural_sorted = miscutil.natural_sort(mm_outs)
    mm_outs = mm_logs_natural_sorted
    keynames = []

    for out in mm_outs:
        if program == 'amber':
            energies_dict = extract_amber_energies(out, forcefield=forcefield)
        elif program == 'gromacs':
            raise Exception("Gromacs is not yet implemented for extracting energies.")
            energies_dict = extract_gromacs_energies(out, forcefield=forcefield)
        energies = [energies_dict]

        with open(str(filename), 'a') as f:
            writer = csv.DictWriter(f, energies[0].keys())
            for e in energies:
                writer.writerow(e)

    for key, value in energies_dict.items():
        keynames.append(key)

    # Reverse ordering in order to properly add column headers
    with open(str(filename)) as fi, open('{0}/Energies_raw_2.txt'.format(write_dir), 'w') as fo:
        fo.writelines(reversed(fi.readlines()))

    # Add column headers (i.e. keys)
    with open('{0}/Energies_raw_2.txt'.format(write_dir), 'a') as f:
        writer = csv.writer(f)
        writer.writerow(keynames)

    # Undo the above reverse to re-obtain proper sequence.
    with open('{0}/Energies_raw_2.txt'.format(write_dir)) as fi, open(str(filename), 'w') as fo:
        fo.writelines(reversed(fi.readlines()))

    os.remove('{0}/Energies_raw_2.txt'.format(write_dir))
