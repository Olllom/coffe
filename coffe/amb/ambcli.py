# -*- coding: utf-8 -*-

"""Console script for coffe's amber specific commands. cli = command line interface."""

from __future__ import absolute_import, division, print_function

import click

from coffe.amb import extract_mm_energies


@click.command()
@click.option('--source_dir', '-sd',
              help='Directory that contains the MM log files (default is to cwd).',
              type=str,
              default=None)
@click.option('--write_dir', '-wd',
              help='Directory that will have the energies written into (default is to cwd).',
              type=str,
              default=None)
@click.option('--filename', '-fn',
              help='The filename for which the energies will be written to.',
              type=str,
              default=None)
@click.option('--forcefield', '-ff',
              help='The name of the force field that was employed (e.g. gaff, glycam06) (defaults is to MM).',
              type=str,
              default='MM')
@click.option('--program', '-p',
              help='[amber | gromacs (coming soon)] The target program (default is to guess, which may fail).',
              type=str,
              default=None)
def get_mm_energies(source_dir, write_dir, filename, forcefield, program):
    """Extract all MM raw final energies from outfiles"""
    extract_mm_energies.extract_energies_and_write(source_dir, write_dir, filename, forcefield, program)
