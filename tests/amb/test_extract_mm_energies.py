# -*- coding: utf-8 -*-

"""Test for extracting raw energies from mm calculations.
    Currently supports Amber.
    """

from __future__ import absolute_import, division, print_function

from .. import helper_functions
from coffe.core import pkgdata
from coffe.amb import extract_mm_energies


def test_amber_copt(tmpdir):
    source_dir = pkgdata.abspath('data/00_amber_torsion_profile')
    file_target = str(source_dir) + '/Energies_raw.csv.target'
    file_generated = str(tmpdir) + '/Energies_raw.csv'
    program = 'amber'
    forcefield = 'gaff'
    extract_mm_energies.extract_energies_and_write(source_dir, tmpdir, file_generated, forcefield, program)
    assert helper_functions.are_csv_files_equal(file_generated, file_target)
