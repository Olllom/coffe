# -*- coding: utf-8 -*-

"""Test for extracting raw energies from qm calculations.
    Currently supports GAMESS and Psi4."""

from __future__ import absolute_import, division, print_function

import glob
import os

from .. import helper_functions
from coffe.core import coffedir
from coffe.core import pkgdata
from coffe.quantum import check_qm_log
from coffe.quantum import extract_qm_energies


def test_psi4_opt_qm10_10(tmpdir):
    """Test to extract two final HF energies optimized using Psi4."""

    work_dir, coffe_dir, logger = coffedir.prepare_coffe_work_dir(str(tmpdir))
    source_dir = pkgdata.abspath('data/00_psi4_opt_qm10-10')
    inputfiles = glob.glob('{0}/*.inp.log'.format(source_dir))
    file_target = '{0}/energies_raw.csv.target'.format(source_dir)

    outputfile = '{0}/energies_raw.csv'.format(work_dir)
    program = 'psi4'

    extract_qm_energies.extract_energies_and_write(inputfiles=inputfiles, outputfile=outputfile,
                                                   program=program, work_dir=work_dir)
    assert helper_functions.are_csv_files_equal(outputfile, file_target)


def test_psi4_opt_qm20_21(tmpdir):
    """A folder that contains 1) minima, and 2) failed jobs. MP2 theory"""

    work_dir, coffe_dir, logger = coffedir.prepare_coffe_work_dir(str(tmpdir))
    source_dir = pkgdata.abspath('data/00_psi4_opt_qm20-21')
    inputfiles = glob.glob('{}/*.inp.log'.format(source_dir))
    file_target = os.path.join(source_dir + '/energies_raw.csv.target').format(source_dir)

    opt_success = []
    for file in inputfiles:
        optimized = check_qm_log.check_qm_optimization(file)
        if optimized == "True":
            opt_success.append(file)

    outputfile = '{0}/energies_raw.csv'.format(work_dir)
    program = 'psi4'

    extract_qm_energies.extract_energies_and_write(inputfiles=opt_success, outputfile=outputfile,
                                                   program=program, work_dir=work_dir)
    assert helper_functions.are_csv_files_equal(outputfile, file_target)


def test_psi4_opt_qm71_21(tmpdir):
    """A folder that contains 1) minima, and 2) failed jobs. DFT theory"""

    work_dir, coffe_dir, logger = coffedir.prepare_coffe_work_dir(str(tmpdir))
    source_dir = pkgdata.abspath('data/00_psi4_opt_qm71_21')
    inputfiles = glob.glob('{0}/*-psi.log'.format(source_dir))
    file_target = os.path.join(source_dir + '/energies_raw.csv.target').format(source_dir)

    opt_success = []
    for file in inputfiles:
        optimized = check_qm_log.check_qm_optimization(file)
        if optimized == "True":
            opt_success.append(file)

    outputfile = '{0}/energies_raw.csv'.format(work_dir)
    program = 'psi4'

    extract_qm_energies.extract_energies_and_write(inputfiles=opt_success, outputfile=outputfile,
                                                   program=program, work_dir=work_dir)
    assert helper_functions.are_csv_files_equal(outputfile, file_target)


def test_psi4_spc_qm2021_3521(tmpdir):
    """A folder that contains single point calculations computed at MP2.5 using Psi4."""

    work_dir, coffe_dir, logger = coffedir.prepare_coffe_work_dir(str(tmpdir))
    source_dir = pkgdata.abspath('data/01_psi4_spc_qm20-21_35-21')
    inputfiles = glob.glob('{}/*.inp.log'.format(source_dir))
    file_target = os.path.join(source_dir + '/energies_raw.csv.target').format(source_dir)

    outputfile = '{0}/energies_raw.csv'.format(work_dir)
    program = 'psi4'

    extract_qm_energies.extract_energies_and_write(inputfiles=inputfiles, outputfile=outputfile,
                                                   program=program, work_dir=work_dir)
    assert helper_functions.are_csv_files_equal(outputfile, file_target)


def test_psi4_spc_qm2021_6121(tmpdir):
    """A folder that contains single point calculations computed at CCSD(T) using Psi4."""

    work_dir, coffe_dir, logger = coffedir.prepare_coffe_work_dir(str(tmpdir))
    source_dir = pkgdata.abspath('data/01_psi4_spc_qm20-21_61-21')
    inputfiles = glob.glob('{0}/*.inp.log'.format(source_dir))
    file_target = os.path.join('{0}/energies_raw.csv.target').format(source_dir)

    outputfile = '{0}/energies_raw.csv'.format(work_dir)
    program = 'psi4'

    extract_qm_energies.extract_energies_and_write(inputfiles=inputfiles, outputfile=outputfile,
                                                   program=program, work_dir=work_dir)
    assert helper_functions.are_csv_files_equal(outputfile, file_target)


def test_psi4_spc_qm2021_20cbs34(tmpdir):
    """A folder that contains single point calculations computed at MP2/CBS using Psi4."""

    work_dir, coffe_dir, logger = coffedir.prepare_coffe_work_dir(str(tmpdir))
    source_dir = pkgdata.abspath('data/01_psi4_spc_qm20-21_20-cbs34')
    inputfiles = glob.glob('{0}/*.inp.log'.format(source_dir))
    file_target = os.path.join('{0}/energies_raw.csv.target').format(source_dir)

    outputfile = '{0}/energies_raw.csv'.format(work_dir)
    program = 'psi4'

    extract_qm_energies.extract_energies_and_write(inputfiles=inputfiles, outputfile=outputfile,
                                                   program=program, work_dir=work_dir)
    assert helper_functions.are_csv_files_equal(outputfile, file_target)


def test_gamess_opt_qm10_10(tmpdir):
    """A folder that contains HF 1) minima and 2) failed jobs using GAMESS."""

    work_dir, coffe_dir, logger = coffedir.prepare_coffe_work_dir(str(tmpdir))
    source_dir = pkgdata.abspath('data/00_gamess_opt_qm10-10')
    inputfiles = glob.glob('{0}/*.inp.log'.format(source_dir))
    file_target = os.path.join('{0}/energies_raw.csv.target').format(source_dir)

    opt_success = []
    for file in inputfiles:
        optimized = check_qm_log.check_qm_optimization(file)
        if optimized == "True":
            opt_success.append(file)

    outputfile = '{0}/energies_raw.csv'.format(work_dir)
    program = 'gamess'

    extract_qm_energies.extract_energies_and_write(inputfiles=opt_success, outputfile=outputfile,
                                                   program=program, work_dir=work_dir)
    assert helper_functions.are_csv_files_equal(outputfile, file_target)


def test_gamess_opt_qm20_31(tmpdir):
    """A folder that contains 1) minima, 2) failed jobs, and 3) transition states."""

    work_dir, coffe_dir, logger = coffedir.prepare_coffe_work_dir(str(tmpdir))
    source_dir = pkgdata.abspath('data/00_gamess_opt_qm20-31')
    inputfiles = glob.glob('{0}/*.inp.log'.format(source_dir))
    file_target = os.path.join('{0}/energies_raw.csv.target').format(source_dir)

    opt_success = []
    for file in inputfiles:
        optimized = check_qm_log.check_qm_optimization(file)
        if optimized == "True":
            opt_success.append(file)

    outputfile = '{0}/energies_raw.csv'.format(work_dir)
    program = 'gamess'

    extract_qm_energies.extract_energies_and_write(inputfiles=opt_success, outputfile=outputfile,
                                                   program=program, work_dir=work_dir)
    assert helper_functions.are_csv_files_equal(outputfile, file_target)


def test_gamess_spc_qm1010_2020(tmpdir):
    """A folder that contains MP2 single point calculations using GAMESS."""

    work_dir, coffe_dir, logger = coffedir.prepare_coffe_work_dir(str(tmpdir))
    source_dir = pkgdata.abspath('data/01_gamess_spc_qm10-10_20-20')
    inputfiles = glob.glob('{0}/*.inp.log'.format(source_dir))
    file_target = os.path.join('{0}/energies_raw.csv.target').format(source_dir)

    outputfile = '{0}/energies_raw.csv'.format(work_dir)
    program = 'gamess'

    extract_qm_energies.extract_energies_and_write(inputfiles=inputfiles, outputfile=outputfile,
                                                   program=program, work_dir=work_dir)
    assert helper_functions.are_csv_files_equal(outputfile, file_target)
