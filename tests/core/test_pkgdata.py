# -*- coding: utf-8 -*-

"""Tests for coffe.core.relpath functions"""

from __future__ import absolute_import, division, print_function
import os
from coffe.core import pkgdata


def test_abspath():
    filename = pkgdata.abspath("data/foo.txt")
    assert os.path.isfile(filename)
