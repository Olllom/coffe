# -*- coding: utf-8 -*-

"""Tests for coffe.analysis.gyration_radius functions"""

from __future__ import absolute_import, division, print_function

import os
import filecmp

from coffe.core import coffedir
from coffe.core import pkgdata
from coffe.analysis import gyration_radius


def test_gyrad_pdb_csv(tmpdir):
    """
    Tests for computing radius of gyration on individual pdb files and creating
    an gyrad.csv output file.
    """
    directory_target = "{0}".format(tmpdir)
    work_dir, coffe_dir, logger = coffedir.prepare_coffe_work_dir(
        directory_target)

    structures = ['data/molecule-1.pdb', 'data/molecule-2.pdb',
                  'data/molecule-3.pdb', 'data/molecule-4.pdb']
    structures = [pkgdata.abspath(s) for s in structures]
    gyrads = [1.736, 1.757, 1.743, 1.767]

    # Writing the target
    file_target = "{0}/gyrad_pdb.csv.target".format(str(work_dir))
    with open(file_target, 'w') as file:
        file.write("entity,gyration radius\n")
        for i in range(len(structures)):
            file.write(os.path.basename(structures[i]) + "," + str(gyrads[i]) + "\n")

    file_generated = "{0}/gyrad.csv".format(str(work_dir))
    gyrad_rounded = gyration_radius.gyration_radius_csv(
        structures, file_generated, str(tmpdir))

    assert os.path.isfile(file_generated)
    assert filecmp.cmp(file_generated, file_target)
    assert gyrads == gyrad_rounded

# An alternative approach:
# import pandas
#     file_target = pkgdata.abspath("data/gyrad_pdb.csv.target")
#     file_generated = "{0}/gyrad.csv".format(str(work_dir))
#
#     gyration_radius.gyration_radius_csv(structures, file_generated, work_dir)
#
#     df_target = pandas.read_csv(file_target, sep=',')
#     df_generated = pandas.read_csv(file_generated, sep=',')
#     df_target['entity'] = df_target['entity'].apply(lambda x: os.path.splitext(os.path.basename(x))[0])
#     df_generated['entity'] = df_generated['entity'].apply(lambda x: os.path.splitext(os.path.basename(x))[0])
#
#     assert os.path.isfile(file_generated)
#     assert df_target.equals(df_generated)

def test_gyrad_mol2_csv(tmpdir):
    """
    Tests for computing radius of gyration on individual mol2 files and creating
    an gyrad.csv output file.
    """
    directory_target = "{0}".format(tmpdir)
    work_dir, coffe_dir, logger = coffedir.prepare_coffe_work_dir(
        directory_target)

    structures = ['data/molecule-1.mol2', 'data/molecule-2.mol2',
                  'data/molecule-3.mol2', 'data/molecule-4.mol2']
    structures = [pkgdata.abspath(s) for s in structures]
    gyrads = [1.736, 1.757, 1.743, 1.767]

    # Writing the target
    file_target = "{0}/gyrad_mol2.csv.target".format(str(work_dir))
    with open(file_target, 'w') as file:
        file.write("entity,gyration radius\n")
        for i in range(len(structures)):
            file.write(os.path.basename(structures[i]) + "," + str(gyrads[i]) + "\n")

    file_generated = "{0}/gyrad.csv".format(str(work_dir))
    gyrad_rounded = gyration_radius.gyration_radius_csv(
        structures, file_generated, str(tmpdir))

    assert os.path.isfile(file_generated)
    assert filecmp.cmp(file_generated, file_target)
    assert gyrads == gyrad_rounded
